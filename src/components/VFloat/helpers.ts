export function on(
  element: Element | Document,
  event: string,
  handler: any,
  options: any = false
) {
  if (element && event && handler) {
    element.addEventListener(event, handler, options);
  }
}
