import { ref, onMounted, onUnmounted, onUpdated, watch, nextTick } from "vue";
import { computePosition, offset, shift, arrow } from "@floating-ui/dom";

import { on as onEvent } from "./helpers";
import { Trigger } from "./types";

const HOVER_DELAY = 200;
export function useFloating(triggerRef: any, floatRef: any, options?: any) {
  const {
    trigger = "hover",
    placement = "top",
    arrowRef,
    offset: offsetProp = 8,
    triggerHide,
  } = options;
  let onHideController: AbortController, documentController: AbortController;
  const controller = new AbortController();

  function on(..._args: Parameters<typeof onEvent>) {
    const args = _args.concat({
      signal: controller.signal,
    }) as Parameters<typeof onEvent>;

    onEvent(...args);
  }

  const visible = ref(false);
  function doShow() {
    visible.value = true;
    onEvent(document, "keydown", handleEscape, {
      signal: onHideController?.signal,
    });
  }
  function doHide() {
    onHideController?.abort();
    visible.value = false;
  }
  function doToggle() {
    visible.value = !visible.value;
  }

  let timer: any;
  function onHover(e: MouseEvent) {
    clearTimeout(timer);
    timer = setTimeout(() => {
      onHideController = new AbortController();
      doShow();

      nextTick(() => {
        onEvent(floatRef.value, "mouseleave", doHide, {
          signal: onHideController.signal,
        });
      });
    }, HOVER_DELAY);
  }
  function onLeave(e: MouseEvent) {
    clearTimeout(timer);
    if (!visible.value) return;

    timer = setTimeout(() => {
      if (floatRef.value?.contains(e.relatedTarget)) return;

      doHide();
    }, HOVER_DELAY);
  }
  function doCloseForDocument(e: Event) {
    if (triggerRef.value?.contains(e.target)) return;
    if (floatRef.value?.contains(e.target)) return;

    documentController?.abort();
    doHide();
  }

  function handleEscape({ code }: KeyboardEvent) {
    if (code === "Escape") doHide();
  }
  function handleClickOutside() {
    if (visible.value) {
      documentController = new AbortController();
      onEvent(document, "click", doCloseForDocument, {
        signal: documentController.signal,
      });
    } else {
      documentController.abort();
    }
  }
  function doClickToggle() {
    doToggle();
    handleClickOutside();
  }

  function handleTriggers() {
    [].concat(trigger).forEach((t: Trigger) => {
      switch (t) {
        case "toggle": {
          on(triggerRef.value!, "click", doClickToggle);
          break;
        }

        case "hover": {
          on(triggerRef.value, "mouseover", onHover);
          on(triggerRef.value, "mouseleave", onLeave);
          break;
        }

        case "focus": {
          on(triggerRef.value, "focus", doShow);
          on(triggerRef.value, "blur", doHide);
          break;
        }
      }
    });
  }

  onMounted(() => {
    if (!triggerRef.value) return;

    handleTriggers();
  });
  onUnmounted(() => {
    controller.abort();
    onHideController?.abort();
    documentController?.abort();
  });

  async function update() {
    const {
      x,
      y,
      placement: flippedPlacement,
      middlewareData,
    } = await computePosition(triggerRef.value, floatRef.value!, {
      strategy: "fixed",
      placement,
      middleware: [
        shift({ padding: 8 }),
        ...(arrowRef && [
          offset(offsetProp),
          arrow({ element: arrowRef.value }),
        ]),
      ],
    });

    Object.assign(floatRef.value.style, {
      transform: `translate(${Math.round(x)}px,${Math.round(y)}px)`,
    });

    if (arrowRef) {
      const staticSide = {
        top: "bottom",
        right: "left",
        bottom: "top",
        left: "right",
      }[flippedPlacement.split("-")[0]] as string;
      const { x: arrowX, y: arrowY } = middlewareData.arrow as any;
      const transform = {
        top: "rotate(180deg)",
        right: "rotate(-90deg)",
        bottom: "",
        left: "rotate(90deg)",
      }[staticSide] as string;
      const space = {
        top: "-11px",
        bottom: "-11px",
        right: "-16px",
        left: "-17px",
      }[staticSide] as string;

      Object.assign(arrowRef.value.style, {
        left: arrowX != null ? `${arrowX}px` : "",
        top: arrowY != null ? `${arrowY}px` : "",
        [staticSide]: space,
        transform: transform,
      });
    }
  }

  onUpdated(() => {
    if (visible.value) update();
  });

  watch([triggerHide], () => {
    if (visible.value) doHide();
  });

  return { visible };
}
