export type Trigger = "hover" | "focus" | "toggle";
